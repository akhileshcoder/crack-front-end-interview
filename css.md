# 1. What is CSS selector
We use CSS selector to get a DOM element, so that we can apply some CSS over them or we can add some action on them.

e.g. we use # for id, dot for class, tag-name for tag etc.
# 2. What are CSS selector priorities

basically more specific element will have more priority then lesser one which is also called as specificity rule.
e.g. ID is more specific then class or first-of-type is more specific then nth-of-type so previous will have more priority to 
get css applied then later one.

# 3. What is media-queries.
# What is responsive design and how will you use media-queries for responsive design.
# Give media queries for standard devices (iPhone, smartPhone, tablets, mini-laptops, laptops, desktop, prrojectors)

media-queries helps us to apply css for specific type of screen/device/resolution. using this we can create responsive design as well so 
our application will behave differently on different size of screen.
e.g. @media only screen and (max-width: 600px and min-width: 320px)

# 4. What are grid structure in Tweeter Bootstrap

Bootstrap's grid system divides whole screen width in 12 columns. we can group the columns together to create wider columns for different screen sizes

e.g. xs (for phones - screens less than 768px wide), sm (for tablets - screens equal to or greater than 768px wide), 
md (for small laptops - screens equal to or greater than 992px wide), lg (larger screen)

# 5. What is BOX module 

box-module says about in our content box first margin do comes, then border, then padding, then content 

# 6. What is flex layout

Flex layout is recently added in CSS3 and most of the time it makes our life very easy while creating complex layout which was previously could 
only be done via javascript, display table or calc. 

# 7. What is difference between position absolute, relative, static, fixed, sticky

- static: it takes place where the element should be present. it is default position.
- absolute: it takes place from adjacent non-static parent.
- relative: it takes place from position it has to be default.
- fixed: it takes place from view-port(visible screen)

# 8. what is difference between dispaly inline, inline-block, block 
- block: it is multiline element
- inline: it is single line element and it honers line-hight etc.
- inline-block: it is single line element and it doesn't honers line-hight.

# 9. what does border-box do?

if box-sizing is set to `border-box`, width of item will be calculated by including border, padding and content.

# 10. difference between "px" , "em" , "rem", "%", "vh", "vw"


# 11. what is retina display, and pixel density

# 12. What is user agent CSS, How we senitize it.

# 13. What is web Accessibility. How we optimise page for deaf, blind, Color Blind people.

# 14. Below is dom Structure
```html
<div class="a">
    <div class="b">
        Hi
    </div>
</div>
```
```css
.a { 
  border: 1px solid red;
  height: 100px;
  width: 100px;
}

.b { 
  border: 1px solid blue;
  height: 50px;
  width: 50px;
}
```
Now Centrally allign class `a` inside body, `b` inside `a` and textNode `Hi`. Centralize meaning vertically and Horizontally.
How many ways you can do it.

## answer
I know 4 ways:
### 1: By using position absolute

```css
.a {
    border: 1px solid red;
    height: 100px;
    width: 100px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
}
.b {
    border: 1px solid blue;
    height: 50px;
    width: 50px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
    text-align: center;
    display: table-column-group;
    padding-top: calc(25% - 8px);
    box-sizing: border-box;
    font-size: 16px;
}
```


### 2: By using calc
### 3: By using display table/table-cell
### 4. Best way: using flex
```css
body {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;
    margin: 0;
}
.a {
    border: 1px solid red;
    height: 100px;
    width: 100px;
    display: flex;
    align-items: center;
    justify-content: center;
}
.b {
    border: 1px solid blue;
    height: 50px;
    width: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
}
```

# 15. Below is dom Structure

```html
<div class ="a">
  <div>1</div>
  <div>2</div>
  <div>3</div>
  <div>1</div>
</div>
```
divide child div width in-respect to their values

## Answer
I know 2 ways:
### 1. Using %age and var (complex)

```css
.a {
  width: 100%;
}
.a>* {
    display: inline-block;
    border: 1px solid red;
    height: 100px;
    float: left;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

:root {
  --total-width-sum: (1 + 2 + 3 + 1);
}

.a>*:first-of-type {
  width: calc((100% / var(--total-width-sum)) * 1)
}

.a>*:nth-of-type(2) {
  width: calc((100% / var(--total-width-sum)) * 2)
}

.a>*:nth-of-type(3) {
  width: calc((100% / var(--total-width-sum)) * 3)
}

.a>*:last-of-type {
  width: calc((100% / var(--total-width-sum)) * 1)
}
```

### 2. Using flex

```css
.a {
  width: 100%;
  display: flex;
}
.a>* {
    border: 1px solid red;
    height: 100px;
}
.a>*:first-of-type {
  flex: 1;
}
.a>*:nth-of-type(2) {
  flex: 2;
}
.a>*:nth-of-type(3) {
 flex: 3;
}
.a>*:last-of-type {
  flex: 1;
}
```
