# 1. what is closure
ES5 has three kind of scope 1st global 2nd functional leval 3rd is closure.
it says that variable created inside outer function can be used inside inner function.

Benefits: 

1. we can create private variable by using it.
2. we can create closure for async methods if they are in loop.

Cons:
1. since scope of variable keeps alive they are not getting garbase collected so more memory uses will be there.

# 2. what variable and function hoisting, and what are differences between them.
variable defined or created any where in scope there *definition* goes on top is termed as variable hoisting.

function defined or created any where in scope there *definition* and *implementation* too goes on top is termed as function hoisting.

# 3. what is difference between var, let, const
var : used for variable creation and has only three scope global,functional, closure.

let : used for variable creation but its scope is valid inside block only variable created by using it can be registered for 
GC(garbage collection) after coming out of the block.

const : used for variable creation and is similar as final in java once initialised can not change its value.

# 4. what is difference between call, apply and bind
these all method are being used for attaching the context(value of `this`) but bind does not expect any argument where call expect 
object of argument and apply expect array of argument.

When I'm not having any argument I'll use bind. when I'm not certain for the length of argument I'll use apply else if I know all argument 
I'll use call.

# 5. what is promise.
# - implement promise using es6 or es5. 
# - implement promise.all using es6 or es5.
# - benefit of using promises over callback (what is callback hell)
# - what is async await how to convert promise to async await, with try/catch block to handling errors as we have in catch method with promise.

## Answer:
A promise is used to track a task which will execute in future and can go in two phases e.g. resolved/rejected

For other part search google.

# 6. what all http method are there and their significance
Get: It is being used to fetch the data.
Post: We use it when we want to send some data to server and want something in return.
Put: It is being just to put some data on server.
delete: It is being used to delete some data on server.
Option: For other purpose we use it like validating tokens etc.

Note: These are just for standards. We are not strictly bound to use them for given purpose.

# 7. what is the output of following code and how you will rectify it
```
for(var i = 0; i<100;i++){
    setTimeout(function(){
        console.log(i);
    },1000);
} 
```

It will print 100 hundred times because loop will get executed initially and callback passed to setTimeout method will be pushed to the end of 
event loop. so they will executed when `i` will be already set to its limit i.e. 100

We can reactify it in following ways:

- Using closure
```javascript
for(var i = 0; i<100;i++){
   (function(i){
       setTimeout(function(){
              console.log(i);
       },1000);
   })(i)
} 
```
- Using let keyword
```javascript
for(let i = 0; i<100;i++){
    setTimeout(function(){
        console.log(i);
    },1000);
} 

```

- Using call/apply 
 ```javascript
    // search google
```

# 8. what is major difference between ECMA5 and ECMA6
See link (http://es6-features.org/#Constants)

Classes, Arrow Functions, Block Scope using let keyword, Promises etc


# 9. what is ECMA
ECMA is an organisation who standardise javascript.

# 10. Why javascript is your favorite language
It is my favorite language because of its simplicity and diversity to use. If I know javascript I can work on any part of application 
Front-End(react, angular etc.), TDD testing(kerma, mocha, chai), BDD testing(protractor), backend or micro-services using nodeJS, 
DataBase in MongoDB, IOT or embeded system using jerryscript etc.  

# 11. what is callback hell and how you will remove it

long or iterative nesting of asynchronous functions is known as callback hell.

We can remove it in two ways:
 1. By using promises
 2. By using async await

# 12. what is anonymous functions?

# 13. what is prototype
# - what all ways we can implement OOPS(Object Oriented Programming System) like inheritance, encapsulation, Polymorphism, abstraction.
# - what is prototypical inheritance


# 14. what is prototype chain

# 15. what is difference between arrow functions and normal/regular functions

# 16. what is difference between function expression and function keyword

# 17. implement promise, apply, call, bind using es5 (Very important for senior software engineer)

# 18.  what is rest parameters

# 19. what is destructuring in javascript

# 20. what are generator functions in javascript (Only asked if technical architect of higher position)

# 21. implement Array.concat, Array.forEach, Array.map, Array.flat using es5

# 22. Algorithmic question
## - Find string is palindrome or not

## - Find number is a prime number or not

## - Render Pyramid of `*` e.g. [Sample](https://www.tutorialstonight.com/js/javascript-star-pattern.php#:~:text=Pyramid%20star%20pattern%20in%20javascript,-*%20***%20***&text=It%20uses%202%20loops%20inside,is%202%20*%20i%20%2D%201%20)

## - Find no of island e.g. [Sample](https://dev.to/rattanakchea/amazons-interview-question-count-island-21h6)

## - Flatten array without using Array.prototype.flat

## - Find second highest number in array of sorted and un sorted array (Optimise way)

## - Find word with second highest frequency in a paragraph

## - Split word with vowels with and without using regular expression

## - There are some petrol pump in circle. They have Total fuel to just cover round journey by a vehicle.
## Where should vehicle start trip so he can complete whole circle from start. 
## Write a programme for it.

## - Find pairs of number from given array whose sum is 10.

## - Find Pairs: [Sample](https://javascript.plainenglish.io/algorithms-101-find-pairs-in-javascript-72a1f5e9e68f)

# 23. What is the difference between local storage, cookies and session storage? where you use what.

# 24. what is JWT tokens. Benefits of using JWT tokens over session. What is best place to store JWT tokens, or secure items.

# 25. what will be output of following:

## 1. concept of `variable hoisting`
```javascript
var x = 5;
console.log(x);
function a() {
    console.log(x);
    var x = 6;
    console.log(x);
    return x;
}
console.log(x)
console.log(a())
```
do by yourself

## 2.
### concept of `use strict`
```javascript
'use strict';
var x = 5;
console.log(x);
function a() {
    console.log(x);
    var x = 6;
    console.log(x);
    return x;
}
console.log(x)
console.log(a())
```
### concept of `use strict`
```javascript
"use strict";
console.log(x);
var x = 5;
```
do by yourself

# 3. Concept of `this`
```javascript
console.log(this);
[5].forEach((i) => {
    console.log(this.length);
})
[5].forEach(function (i) {
    console.log(this.length);
})
var a = {
    x1: () => {
        console.log(this);
    },
    x2: function () {
        console.log(this);
    }
}
a.x1();

var temp = () => {
    this.x1();
}
temp();
temp.apply(a, [])

var temp1 = () => {
    this.x1();
}
temp1 = temp1.bind(a);
temp1()
```

# what is lexical scope/environment

# What are polyfills.

# what is v8 engine

# what all javascript engines are their in market and which all browsers use them.

