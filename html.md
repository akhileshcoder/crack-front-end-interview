# 1.what is the difference between HTML4 and HTML5?

if we add Doctype html at start of page then browser will render your page as HTML5/6 else it will be treated as HTML4.

Several new tags got introduced in HTML5 e.g. canvas, audio, video etc.

Basically HTML4 is being used by HTML email bcz most of email vendor does not support HTML5 or later.

# 2.what is difference HTML5 and HTML6

Developers can also use HTML6 to specify locations, times, dates, selling products, bibliographies, and etc.


# 3. what is semantic HTML

Semantic HTML or semantic markup is HTML that introduces meaning to the web page rather than just presentation.
semantic tags make it clear to the browser what the meaning of a page and its content is.

e.g. if user uses header, section, footer. header says the SEO BOT that this will be title for 
the content and section says that it will be description about it.

# 4. How you will make your application SEO(Search engine optmision) friendly

There are Two Ways to Make your One Page Application SEO-Friendly.

1. On Page SEO

2. Off Page SEO

On Page Seo Means Add Meta Keywords, Titles, Descriptions this all are On Page SEO.

Off Page SEO Means Social Bookmarking,Social Sharing Of Your Website.

# 5. What is DOM and DOM tree

DOM stands for Document Object Module where All HTML nodes/tags stored in format of DOM and since all DOM elements are stored in 
tree data structure so collectively they are called DOM tree.


# 6. What is XSS attack

Cross-site Scripting (XSS) refers to client-side code injection attack wherein an attacker can execute malicious scripts 
(also commonly referred to as a malicious payload) into a website.

solution for it is your script always should inject content in your page after validation or sensitization

# 6. What data structure is used for storing documents in html

tree

# 7. What is meta tags used for

It stores extra information page, which helps Search Engine to find details about particular web page